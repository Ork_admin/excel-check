import numpy as np
import pandas as pd
import sys

template_file = sys.argv[1]
checked_file = sys.argv[2]

df_template = pd.read_excel(template_file, index_col=None, header=0)
df_template = df_template.replace(np.nan, 'NAN', regex=True)

df2 = pd.read_excel(checked_file, index_col=None, header=0)
df2 = df2.replace(np.nan, 'NAN', regex=True)

check_flag = df_template.equals(df2)

if check_flag:
    print('Same!')
else:
    print('Difference!')

    comparison_values = df_template.values == df2.values
    rows,cols = np.where(comparison_values==False)
    for item in zip(rows, cols):
        df_template.iloc[item[0], item[1]] = '{} --> {}'.format(df_template.iloc[item[0], item[1]], df2.iloc[item[0], item[1]])
    print('write to Excel_diff.xlsx!')
    df_template.to_excel('Excel_diff.xlsx',index=False,header=True)
