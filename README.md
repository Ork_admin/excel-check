# Excel-check

#### 介绍
Check if two EXCEL table is same 

### 运行
![输入图片说明](https://images.gitee.com/uploads/images/2020/0830/185453_0bee2de2_17062.png "屏幕截图.png")

不同会保存到Excel_diff.xlsx文件内，不同处会以 `-->` 标识，如下：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0830/185515_a08ddc90_17062.png "屏幕截图.png")
